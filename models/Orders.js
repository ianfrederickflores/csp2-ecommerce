const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User Id is required"]
	},

	productId: {
		type: String,
		required: [true, "Product Id is required"]
	},	

	totalAmount: {
		type: Number,
		required: [true, "Total Order Amount is required."]
	},

	totalPrice: {
		type: Number,
		required: [true, "Price is required."]
	},

	status: {
		type: String,
		default: "Pending"
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},	
});

module.exports = mongoose.model("Orders", orderSchema);