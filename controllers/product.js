const Product = require("../models/Products");

// Create New Product
module.exports.addProduct = async (user, reqBody) => {
	if(user.isAdmin){
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newProduct.save().then((course, err) =>{
			if(err){
				return false;
			}

			else{
				return true;
			}
		})
	}

	else{
		return(`You have no access.`)
	}
};

// Retrieve a single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};

// Retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
};

// Update Product Information
module.exports.updateProduct = async (user, reqParams, reqBody) => {
	if(user.isAdmin){
		let updatedProduct = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, err) => {
			if(err){
				return false;
			}
			
			else{
				return true;
			}
		})
	}

	else{
		return (`You have no access`);
	}
};

// Archive Product
module.exports.archiveProduct = async (user, reqParams) => {

	if(user.isAdmin){
		let updateActiveField = {
			isActive : false
		};

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((course, error) => {
			if (error) {
				return false;
			} 

			else{
				return true;
			}

		})
	}

	else{
		return (`You have no access`);
	}
	
};

