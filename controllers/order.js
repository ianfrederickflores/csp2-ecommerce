const Order = require("../models/Orders");
const User = require("../models/Users");
const Product = require("../models/Products");

// Create New Order
module.exports.addOrder = async (user, userId, data) => {
	if(user.isAdmin === false){
		let newOrder = new Order({
			totalAmount: data.totalAmount,			
			totalPrice: data.totalPrice,
			userId: userId,
			productId: data.productId
		})

		return newOrder.save().then((order, err) => {
			if(err){
				return false;
			}
			else{
				return true;
			}
		})
		}

		else{
			return (`You have no access`);
		}	
}

// Retrieve authenticated user's order
module.exports.getOrder = async (user) => {
	if(user.isAdmin === false){
		return Order.find({userId: user.Id}).then(result => {
		if(result == null){
			return false
		}
		else{
			return result;
		}		
	})
	}

	else{
		return(`You have no access.`)
	}
	
};

// Retrieve all orders
module.exports.getAllOrders = async (user) => {
	if(user.isAdmin){
		return Order.find({}).then (result => {
		return result;
		})
	}

	else{
		return (`You have no access`);
	}
	
}